const express = require('express')
const cors = require('cors')
const axois = require('axios')
const TWITTER_API_URL = 'https://api.twitter.com/1.1/statuses/user_timeline.json?count=1&screen_name='
const DEFAULT_USERNAMES = ['brunoraljic', 'mrFunkyWisdom']
const port = 5002
const app = express()

const headers = {
  'Authorization': `Bearer ${process.env.TWITTER_ACCESS_TOKEN}`
};

app.get('/api/track', cors({ origin: '*' }), async (req, res) => {
  try {
    const results = await requestData(req.query);
    res
      .status(200)
      .json(mergeResult(results))
  } catch (err) {
    res
      .status(500)
      .json({
        message: err.message,
        config: err.config
      })
  }
});

const getUsernames = params => {
  if (params && params.usernames) {
    return params.usernames.split(',')
  } else {
    return DEFAULT_USERNAMES;
  }
}

///////// select data
const selectData = data => {
  if (!data || !data[0]) return {}
  const _lastTweet = data[0];
  let selectedData = {
    name: _lastTweet.user.name,
    screenName: _lastTweet.user.screen_name,
    followers: _lastTweet.user.followers_count,
    friendsCount: _lastTweet.user.friends_count,
    favouritesCount: _lastTweet.user.favourites_count,
    statusesCount: _lastTweet.user.statuses_count,
    profileImageUrl: _lastTweet.user.profile_image_url,
    lastTweet: {
      id: _lastTweet.id,
      dateCreated: _lastTweet.created_at,
      text: _lastTweet.text,
      favoriteCount: _lastTweet.favorite_count,
      url: `https://twitter.com/i/web/status/${_lastTweet.id_str}`
    }
  }
  return selectedData;
}

const requestData = async paramUsernames => {
  const userNames = getUsernames(paramUsernames);
  const promises = userNames.map(async userName => {
    const response = await axois.get(TWITTER_API_URL + userName, { headers })
    return {
      [userName]: selectData(response.data)
    }
  })
  return await Promise.all(promises);
}

const mergeResult = results => {
  let finalResult = {};
  results.map(result => {
    finalResult = Object.assign({}, finalResult, result);
  })
  return finalResult;
}

app.listen(port, () => console.log(`Scraper runs on port ${port}!`))
